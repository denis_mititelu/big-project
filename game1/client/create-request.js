function createRequest(method, url, data) {
    var xhr = new XMLHttpRequest();
    xhr.open(method, url);
    xhr.onload = function () {
        var responseText = xhr.responseText;
        console.log(responseText);
        displayResponse(responseText);
    }
    xhr.send(data);
}

function displayResponse (response) {
    var responseContainer = document.getElementById('response');
    responseContainer.innerHTML = response;
}